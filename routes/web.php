<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', ['uses' => 'Project\ProjectController@switch', 'as' => 'project/switch']);
Route::post('/_post/createproject', ['uses' => 'Project\ProjectController@createproject', 'as' => 'project/create']);

Auth::routes();

Route::group(['middleware' => ['auth', 'project.exists']], function () {
    
    Route::get('/', ['uses' => 'Project\ProjectController@overview', 'as' => 'project/overview']);
    
    
    Route::get('/tasks', ['uses' => 'Project\TaskController@listAllTasks', 'as' => 'project/tasks']);
    Route::get('_get/getalltasks', ['uses' => 'Project\TaskController@getAllTasks', 'as' => '_post/getalltasks']);
    
    Route::post('_post/createtask', ['uses' => 'Project\TaskController@createTask', 'as' => '_post/createtask']);

});