<?php

// Project
Breadcrumbs::register('projects', function($breadcrumbs)
{
    $breadcrumbs->push('Projects');
});

// Project -> {project}
Breadcrumbs::register('project', function($breadcrumbs)
{
    $breadcrumbs->parent('projects');
    $breadcrumbs->push(Auth::user()->currentProject()->model->name, route('project/overview'));
});

// Project -> {project} -> Overview
Breadcrumbs::register('project/overview', function($breadcrumbs)
{
    $breadcrumbs->parent('project');
    $breadcrumbs->push('Project Overview', route('project/overview'));
});

// Project -> {project} -> Tasks
Breadcrumbs::register('project/tasks', function($breadcrumbs)
{
    $breadcrumbs->parent('project');
    $breadcrumbs->push('All Tasks', route('project/tasks'));
});