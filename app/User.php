<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Alsek\Project\Models\Task;
use Alsek\Project\Models\ProjectUser;
use Alsek\Project\Models\Project;

class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'company', 'email', 'address', 'city', 'country', 'postcode', 'password', 'current_project',
    ];
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['firstname', 'lastname', 'email', 'creator'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * 
     * @return String containing either just a URL or a complete image tag
     */
    public function get_gravatar( $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = [] ) {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $this->email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }
    
    public function hasProject()
    {
        return $this->activeProjects()->count() > 0;
    }
    
    public function fullname()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
    
    public function switchProject( Int $id )
    {
        $project = $this->projects()->where('project_id', $id)->first();
        
        if($project == null)
        {
            return $this->update([
                'current_project' => null
            ]);
        }
        return $this->update([
            'current_project' => $project->model->id
        ]);
    }
    
    /**
     * Relationships
     * 
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'created_by');
    }
    
    public function projects()
    {
        return $this->hasMany(ProjectUser::class, 'user_id');
    }
    
    public function activeProjects()
    {
        return $this->projects()->where('status', 1)->get();
    }
    
    public function currentProject()
    {
        return $this->activeProjects()->where('project_id', $this->current_project)->first();
    }
}
