<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alsek\Project\Repositories\TaskRepository;
use DateTime;
use Auth;

class TaskController extends Controller
{
    protected $taskRepo;
    
    public function __construct(TaskRepository $taskRepo)
    {
        $this->taskRepo = $taskRepo;
    }
    
    public function listAllTasks()
    {
        $currentProject = Auth::user()->currentProject();
        
        return view('task.listalltasks', [
            
        ]);
    }
    
    public function getAllTasks()
    {
        $currentProject = Auth::user()->currentProject();
        
        $tasks = $this->taskRepo->getAllTasksForProject($currentProject->id, ['id', 'created_at', 'summary', 'created_by', 'status']);
        
        return response()->json(["data" => $tasks->toArray()]);
    }
}
