<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alsek\Project\Repositories\ProjectRepository;
use Alsek\Project\Models\Project;
use Auth;

class ProjectController extends Controller
{
    protected $projectRepo;
    
    public function __construct(ProjectRepository $projectRepo)
    {
        $this->projectRepo = $projectRepo;
    }
    
    public function createProject(Request $request)
    {
        $project = Project::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'created_by' => Auth::user()->id
        ]);
        
        Auth::user()->projects()->create([
            'role' => 1,
            'status' => 1,
            'project_id' => $project->id,
            'user_id' => Auth()->user()->id,
            'created_by' => Auth::user()->id
        ]);
        
        Auth::user()->switchProject($project->id);
        
        return redirect()->route('project/overview');
    }
    
    public function overview()
    {
        $currentProject = Auth::user()->currentProject();
        
        return view('project.overview', [
            'title' => $currentProject->model->name . ' Overview'
        ]);
    }
    
    public function switch(Request $request)
    {
        Auth::user()->switchProject($request->get('project'));
        
        return redirect()->route('project/overview');
    }
}
