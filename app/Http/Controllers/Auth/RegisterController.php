<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Alsek\Project\Models\Project;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:60',
            'lastname' => 'required|max:60',
            'company' => 'required|max:60',
            'email' => 'required|email|max:255|unique:user',
            // 'address' => 'required',
            // 'city' => 'required',
            // 'country' => 'required',
            // 'postcode' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => $data['company'],
            'email' => $data['email'],
            'address' => (isset($data['address']) ? $data['address'] : null),
            'city' => (isset($data['city']) ? $data['city'] : null),
            'country' => (isset($data['country']) ? $data['country'] : null),
            'postcode' => (isset($data['postcode']) ? $data['postcode'] : null),
            'password' => bcrypt($data['password']),
        ]);
        
    }
}
