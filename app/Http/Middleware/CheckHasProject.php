<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckHasProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->projects()->count() == 0 || Auth::user()->currentProject() == null)
        {
            return response()
                ->view('project.choose', ['hasProject' => Auth::user()->hasProject()], 200);
        }
        return $next($request);
    }
}
