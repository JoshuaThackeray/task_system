const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    mix
        
        
        //Metronic assets
        .copy('resources/assets/metronic/global/plugins', 'public/assets/plugins')
        .copy('resources/assets/metronic/pages/img', 'public/assets/vendor/img')
        .copy('resources/assets/metronic/layouts/layout/img', 'public/assets/vendor/img')
        .copy('resources/assets/metronic/global/img', 'public/assets/vendor/img')
        
        .styles(['components.css'], 'public/assets/vendor/css/components.css', 'resources/assets/metronic/global/css')
        .styles(['plugins.css'], 'public/assets/vendor/css/plugins.css', 'resources/assets/metronic/global/css')
        .styles(['layout.css'], 'public/assets/vendor/css/layout.css', 'resources/assets/metronic/layouts/layout/css')
        .styles(['light.css'], 'public/assets/vendor/css/themes/light.css', 'resources/assets/metronic/layouts/layout/css/themes')
        .styles(['custom.css'], 'public/assets/vendor/css/custom.css', 'resources/assets/metronic/layouts/layout/css')
        
        .scripts(['app.js'], 'public/assets/vendor/scripts/app.js', 'resources/assets/metronic/global/scripts')
        .scripts(['layout.js'], 'public/assets/vendor/scripts/layout.js', 'resources/assets/metronic/layouts/layout/scripts')
        .scripts(['demo.js'], 'public/assets/vendor/scripts/demo.js', 'resources/assets/metronic/layouts/layout/scripts')
        .scripts(['quick-sidebar.js'], 'public/assets/vendor/scripts/quick-sidebar.js', 'resources/assets/metronic/layouts/global/scripts')
        .scripts(['quick-nav.js'], 'public/assets/vendor/scripts/quick-nav.js', 'resources/assets/metronic/layouts/global/scripts')
        .scripts(['datatable.js'], 'public/assets/vendor/scripts/datatable.js', 'resources/assets/metronic/global/scripts')
        
        //Page styles
        .styles(['global.css'], 'public/assets/css/global.css', 'resources/assets/css')
        .styles(['login.css'], 'public/assets/css/login.css', 'resources/assets/css')
        .styles(['error.css'], 'public/assets/css/error.css', 'resources/assets/css')
        
        //Page scripts
        .scripts(['login.js'], 'public/assets/scripts/login.js', 'resources/assets/scripts')
        .scripts(['tasks.js'], 'public/assets/scripts/tasks.js', 'resources/assets/scripts')
;});
