var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleDatatable = function () {

        var grid = new Datatable();

        grid.init({
            src: $("#allTasks"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "/_get/getalltasks"
                },
                "language": {
                    "emptyTable": "There are no tasks to show"
                },
                "columns": [
                    { data: 'created_at' },
                    { data: 'summary' },
                    {
                        data: 'creator',
                        render: function (data, type, full, meta ) {
                            return data['firstname']+' '+data['firstname'];
                        }
                    },
                    {
                        data: 'status',
                        render: function (data, type, full, meta ) {
                            switch (data) {
                                case 0:
                                    return '<span class="label label-sm label-default"> Pending </span>';
                                case 1:
                                    return '<span class="label label-sm label-primary"> Todo </span>';
                                case 2:
                                    return '<span class="label label-sm label-success"> Completed </span>';
                                default:
                                    return 'lol';
                            }
                        }
                    },
                    {
                        data: 'id',
                        render: function (data, type, full, meta ) {
                            return '<a href="/task/' + data + '" class="btn btn-sm btn-outline grey-salsa"><i class="fa fa-search"></i> View</a>';
                        }
                    }
                ]
            }
        });

        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        //grid.setAjaxParam("customActionType", "group_action");
        //grid.getDataTable().ajax.reload();
        //grid.clearAjaxParams();
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleDatatable();
        }

    };

}();

$(document).ready(function() {
    TableDatatablesAjax.init(); 
});