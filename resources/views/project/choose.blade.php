@extends('layouts.auth')

@section('form')
    @if( $hasProject )
        {!! Form::open(['url' => '/', 'method' => 'post', 'class' => 'form-horizontal form-row-seperated']) !!}
            <h3 class="form-title font-green">Choose Project</h3>
            <div class="form-body">
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <select name="project" class="bs-select form-control select2 project-change">
                            <option value="" disabled selected>Select...</option>
                            @foreach(Auth::user()->projects as $project)
                                <option value="{{ $project->model->id }}" data-subtext="{{ ($project->role == 1 ? 'Admin' : 'Standard') }}"> {{ $project->model->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
            <div class="clearfix"></div>
            </div>
        {!! Form::close() !!}
    @endif
    {!! Form::open(['url' => '/_post/createproject', 'method' => 'post', 'class' => 'form-horizontal form-row-seperated']) !!}
        <h3 class="form-title font-green">Create Project</h3>
        <div class="form-body">
            <div class="form-group">
                {!! Form::label('name', 'Project Name', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
                {!! Form::text('name', '', ['class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Project Name', 'autocomplete' => 'off']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('description', 'Description', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
                {!! Form::textarea('description', '', ['class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Description', 'autocomplete' => 'off', 'style' => 'resize:none']) !!}
            </div>
            <div class="form-actions">
                <button class="btn btn-sm btn-success" type="submit">Create Project</button>
            </div>
            
        <div class="clearfix"></div>
        </div>
    {!! Form::close() !!}
@endsection