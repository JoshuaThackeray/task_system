@extends('base')

@section('body')
<body class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12 coming-soon-header">
                <a class="brand" href="index.html">
                    {!! Html::image(asset('assets/img/logo.png'), 'logo') !!}
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 coming-soon-content">
                @yield('content')
            </div>
        </div>
        <!--/end row-->
        <div class="row">
            <div class="col-md-12 coming-soon-footer"> {!! config('app.copyright') !!} </div>
        </div>
    </div>
<!-- End -->
@endsection

@section('page-styles')
    {!! Html::style(asset('assets/css/error.css')) !!}
@endsection