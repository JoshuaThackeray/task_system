@extends('base')

@section('body')
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="/">
                        {!! Html::image(asset('assets/img/logo.png'), 'logo', ['class' => 'logo-default']) !!}
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <span></span>
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                
                <form class="search-form search-form-expanded" action="page_general_search_3.html" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search..." name="query">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit">
                                <i class="icon-magnifier"></i>
                            </a>
                        </span>
                    </div>
                </form>
                
                @include('partials.topmenu')
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler">
                                <span></span>
                            </div>
                        </li>
                        <!-- END SIDEBAR TOGGLER BUTTON -->
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <li class="sidebar-search-wrapper">
                            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                            {!! Form::open(['url' => '', 'method' => 'post', 'class' => 'form-horizontal form-row-seperated']) !!}
                                <div class="form-body">
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1">
                                            <select name="project" class="bs-select form-control select2 project-change">
                                                <optgroup label="Projects">
                                                @foreach(Auth::user()->projects as $project)
                                                    <option value="{{ $project->model->id }}" data-subtext="{{ ($project->role == 1 ? 'Admin' : 'Standard') }}" {{ (Auth::user()->currentProject()->model->id == $project->model->id ? 'selected' : '' )}}> {{ $project->model->name }} </option>
                                                @endforeach
                                                </optgroup>
                                                <option value="0" data-subtext="">Create a new project</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                            <!-- END RESPONSIVE QUICK SEARCH FORM -->
                        </li>
                        @include('partials.navbar')
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        @yield('breadcrumbs')
                        
                        <!--<div class="page-toolbar">-->
                        <!--    <div class="btn-group pull-right open">-->
                        <!--        <a class="btn green btn-sm btn-outline" data-toggle="modal" data-target="#newTask">-->
                        <!--            Create new task-->
                        <!--        </a>-->
                        <!--    </div>-->
                        <!--</div>-->
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h1 class="page-title">
                        {{ $title or '' }}
                    </h1>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    @yield('content')
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <?php /*@include('partials.sidebar') */ ?>
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> {!! config('app.copyright') !!} </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
    </div>
    
    
<!-- Modal -->
<div class="modal fade" id="newTask" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Create a new task</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid form">
            {!! Form::open(['url' => '_POST/createtask', 'method' => 'post', 'class' => 'form-horizontal form-row-seperated']) !!}
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Project</label>
                        <div class="col-md-9">
                            {!! Form::select('project', [Auth::user()->currentProject()->model->id =>  Auth::user()->currentProject()->model->name], Auth::user()->currentProject()->model->id, ['class' => 'form-control', 'disabled']) !!}
                            {!! Form::hidden('project', Auth::user()->currentProject()->model->id) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Reporter</label>
                        <div class="col-md-9">
                            {!! Form::select('reporter', [Auth::user()->id => Auth::user()->fullname() . ' (' . Auth::user()->email . ') '], Auth::user()->id, ['class' => 'form-control', 'disabled']) !!}
                            {!! Form::hidden('reporter', Auth::user()->id) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Summary</label>
                        <div class="col-md-9">
                            {!! Form::text('summary', '', ['class' => 'form-control', 'placeholder' => 'A short summary of the task']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Assigned to</label>
                        <div class="col-md-9">
                            <select class="form-control bs-select">
                                <option value="0" selected>Unnassigned</option>
                                @foreach(Auth::user()->currentProject()->model->users as $user)
                                    <option value="{{ $user->id }}">
                                        {{ $user->fullname() . ' (' . $user->email . ') ' }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Priority</label>
                        <div class="col-md-9">
                            {!! Form::select('size', ['0' => 'Lowest', '1' => 'Low', '2' => 'Normal', '3' => 'High', '4' => 'Highest'], '2', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Estimate</label>
                        <div class="col-md-9">
                            {!! Form::text('estimate', '', ['class' => 'form-control', 'placeholder' => 'e.g. 1d 2h ( 1 day and 2 hours ), 4w ( 4 weeks ), etc.']) !!}
                        </div>
                    </div>
                    <div class="form-group last">
                        <label class="control-label col-md-3">Description</label>
                        <div class="col-md-9">
                            {!! Form::textarea('description', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green">
                                Create Task
                            </button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
@endsection