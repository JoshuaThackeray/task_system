@extends('base')

@section('body')
<body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                {!! Html::image(asset('assets/img/logo.png'), 'logo') !!}
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            @yield('form')
        </div>
        <div class="copyright"> {!! config('app.copyright') !!} </div>
</body>
@endsection

@section('page-scripts')
    {!! Html::script(asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')) !!}
    {!! Html::script(asset('assets/plugins/jquery-validation/js/additional-methods.min.js')) !!}
    {!! Html::script(asset('assets/scripts/login.js')) !!}
@endsection

@section('page-styles')
    {!! Html::style(asset('assets/css/login.css')) !!}
@endsection