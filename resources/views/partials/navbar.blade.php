<li class="heading">
    <h3 class="uppercase">Project</h3>
</li>
<li class="nav-item start ">
    <a href="/" class="nav-link nav-toggle">
        <i class="icon-home"></i>
        <span class="title">Overview</span>
    </a>
</li>
<li class="nav-item start ">
    <a href="/tasks" class="nav-link nav-toggle">
        <i class="icon-calendar"></i>
        <span class="title">Tasks</span>
    </a>
</li>
<li class="nav-item start ">
    <a href="/" class="nav-link nav-toggle">
        <i class="icon-layers"></i>
        <span class="title">Groups</span>
    </a>
</li>
@if(Auth::user()->currentProject()->isAdmin())
<li class="nav-item start ">
    <a href="/" class="nav-link nav-toggle">
        <i class="icon-briefcase"></i>
        <span class="title">Admin</span>
    </a>
</li>
@endif
<li class="heading">
    <h3 class="uppercase">Account</h3>
</li>
<li class="nav-item">
    <a href="/" class="nav-link nav-toggle">
        <i class="icon-settings"></i>
        <span class="title">Settings</span>
    </a>
</li>