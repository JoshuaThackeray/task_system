<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
        <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
        <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
        <li class="dropdown dropdown-dark dropdown-extended dropdown-notification" id="header_notification_bar">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="icon-bell"></i>
                <span class="badge badge-default"><!-- Number goes here --></span>
            </a>
            <ul class="dropdown-menu">
                <li class="external">
                    <h3>
                        <span class="bold">0 pending</span> notifications</h3>
                    <a href="/">view all</a>
                </li>
                <li>
                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                        <?php /*<li>
                            <a href="javascript:;">
                                <span class="time">just now</span>
                                <span class="details">
                                    <span class="label label-sm label-icon label-success">
                                        <i class="fa fa-plus"></i>
                                    </span> New user registered. </span>
                            </a>
                        </li> */ ?>
                    </ul>
                </li>
            </ul>
        </li>
        <!-- END NOTIFICATION DROPDOWN -->
        <!-- BEGIN INBOX DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
            <?php /*<li class="dropdown dropdown-dark dropdown-extended dropdown-inbox" id="header_inbox_bar">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-envelope-open"></i>
                    <span class="badge badge-default"><!-- Number goes here --></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="external">
                        <h3>You have
                            <span class="bold">0 New</span> Messages</h3>
                        <a href="/">view all</a>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                            <li>
                                <a href="#">
                                    <span class="photo">
                                        {!! Html::image(asset('assets/vendor/img/avatar2.jpg'), '', ['class' => 'img-circle']) !!}
                                    </span>
                                    <span class="subject">
                                        <span class="from"> Lisa Wong </span>
                                        <span class="time">Just Now </span>
                                    </span>
                                    <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <!-- END INBOX DROPDOWN -->
            <!-- BEGIN TODO DROPDOWN -->
            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
            <li class="dropdown dropdown-dark dropdown-extended dropdown-tasks" id="header_task_bar">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-calendar"></i>
                    <span class="badge badge-default"><!-- Number goes here --></span>
                </a>
                <ul class="dropdown-menu extended tasks">
                    <li class="external">
                        <h3>You have
                            <span class="bold">0 pending</span> tasks</h3>
                        <a href="/">view all</a>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                            <li>
                                <a href="javascript:;">
                                    <span class="task">
                                        <span class="desc">New release v1.2 </span>
                                        <span class="percent">30%</span>
                                    </span>
                                    <span class="progress">
                                        <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">40% Complete</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li> */ ?>
        <!-- END TODO DROPDOWN -->
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-dark dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                {!! Html::image(Auth::user()->get_gravatar(), '', ['class' => 'img-circle']) !!}
                <span class="username username-hide-on-mobile"> {{ Auth::user()->fullname() . ' (' . Auth::user()->company . ') ' }} </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a href="/">
                        <i class="icon-user"></i> My Profile </a>
                </li>
                <li class="divider"> </li>
                <li>
                    <a onclick="document.getElementById('logout-form').submit();"><i class="icon-key"></i> Log Out </a>
                    {!! Form::open(['method' => 'post', 'url' => '/logout', 'id' => 'logout-form']) !!}
                    {!! Form::close() !!}
                </li>
                
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <?php /* <li class="dropdown dropdown-quick-sidebar-toggler">
            <a href="javascript:;" class="dropdown-toggle">
                <i class="icon-logout"></i>
            </a>
        </li> */ ?>
        <!-- END QUICK SIDEBAR TOGGLER -->
    </ul>
</div>
<!-- END TOP NAVIGATION MENU -->