<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>{!! config('app.name') . ' - Task Management System' !!}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Project management software..."
            name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        {!! Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') !!}
        {!! Html::style(asset('assets/plugins/font-awesome/css/font-awesome.min.css')) !!}
        {!! Html::style(asset('assets/plugins/simple-line-icons/simple-line-icons.min.css')) !!}
        {!! Html::style(asset('assets/plugins/bootstrap/css/bootstrap.min.css')) !!}
        {!! Html::style(asset('assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css')) !!}
        {!! Html::style(asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css')) !!}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        {!! Html::style(asset('assets/vendor/css/components.css'), ['id' => 'style_components']) !!}
        {!! Html::style(asset('assets/vendor/css/plugins.css')) !!}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        {!! Html::style(asset('assets/vendor/css/layout.css')) !!}
        {!! Html::style(asset('assets/vendor/css/themes/light.css'), ['id' => 'style_color']) !!}
        {!! Html::style(asset('assets/vendor/css/custom.css')) !!}
        {!! Html::style(asset('assets/css/global.css')) !!}
        <!-- END THEME LAYOUT STYLES -->
        <!-- PAGE STYLES -->
        @yield('page-styles')
        @yield('custom-css')
        <!-- END PAGE STYLE -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

        @yield('body')
        
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        {!! Html::script(asset('assets/plugins/jquery.min.js')) !!}
        {!! Html::script(asset('assets/plugins/bootstrap/js/bootstrap.min.js')) !!}
        {!! Html::script(asset('assets/plugins/js.cookie.min.js')) !!}
        {!! Html::script(asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')) !!}
        {!! Html::script(asset('assets/plugins/jquery.blockui.min.js')) !!}
        {!! Html::script(asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js')) !!}
        {!! Html::script(asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js')) !!}
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        {!! Html::script(asset('assets/vendor/scripts/app.js')) !!}
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        {!! Html::script(asset('assets/vendor/scripts/layout.js')) !!}
        {!! Html::script(asset('assets/vendor/scripts/demo.js')) !!}
        {!! Html::script(asset('assets/vendor/scripts/quick-sidebar.js')) !!}
        {!! Html::script(asset('assets/vendor/scripts/quick-nav.js')) !!}
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript">
            $(".bs-select").selectpicker({
                iconBase: "fa",
                tickIcon: "fa-check"
            });
            
            $(".project-change").on('change', function(event){
                event.preventDefault();
                $(this).closest('form').submit();
            });
        </script>
        <!-- PAGE STYLES -->
        @yield('page-scripts')
        @yield('custom-js')
        <!-- END PAGE STYLE -->
    </body>

</html>