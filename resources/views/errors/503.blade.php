@extends('layouts.error')

@section('content')
<h1>We're down right now!</h1>
<p> We apologise for this maintenance period and we will be up and running again very shortly. </p>
<br>
@endsection