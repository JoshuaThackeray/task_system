@extends('layouts.error')

@section('content')
<h1>I couldn't find the page you were looking for!</h1>
<p> Click the button below to get back to safety. </p>
<a class="btn btn-lg btn-primary" href="/">Back to safety</a>
@endsection