@extends('layouts.main')

@section('breadcrumbs', Breadcrumbs::render('project/tasks'))

@section('page-scripts')
    {!! Html::script(asset('assets/vendor/scripts/datatable.js')) !!}
    {!! Html::script(asset('assets/plugins/datatables/datatables.min.js')) !!}
    {!! Html::script(asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')) !!}
    {!! Html::script(asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')) !!}
    {!! Html::script(asset('assets/scripts/tasks.js')) !!}
@endsection

@section('page-styles')
    {!! Html::style(asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')) !!}
    {!! Html::style(asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')) !!}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-calendar font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">All Tasks</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover" id="allTasks">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="25%"> Created at </th>
                                <th width="35%"> Summary </th>
                                <th width="20%"> Reported by </th>
                                <th width="5%"> Status </th>
                                <th width="5%"></th>
                            </tr>
                            <tr role="row" class="filter">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group col-md-6 date date-picker pull-left" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" readonly name="created_at" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group col-md-6 date date-picker pull-left" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" readonly name="created_at" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="summary"> 
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="reported_by"> 
                                </td>
                                <td>
                                    <select name="status" class="form-control form-filter input-sm">
                                        <option value="">Select...</option>
                                        <option value="Todo">Todo</option>
                                        <option value="In Progress">In Progress</option>
                                        <option value="Completed">Completed</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                            <i class="fa fa-search"></i> Search</button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection