@extends('layouts.auth')

<!-- Main Content -->
@section('form')
<!-- BEGIN FORGOT PASSWORD FORM -->
{!! Form::open(['url' => '/password/email', 'method' => 'post', 'class' => 'form-horizontal forget-form']) !!}
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <h3 class="font-green">Forget Password ?</h3>
    <p> Enter your e-mail address below to reset your password. </p>
    <div class="form-group">
        {!! Form::email('email', '', ['class' => 'form-control placeholder-no-fix', 'autocomplete', 'placeholder' => 'Email']) !!}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-actions">
        <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
        <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
    </div>
{!! Form::close() !!}
<!-- END FORGOT PASSWORD FORM -->
@endsection
