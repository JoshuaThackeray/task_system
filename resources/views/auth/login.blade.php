@extends('layouts.auth')

@section('form')
<!-- BEGIN LOGIN FORM -->
{!! Form::open(['url' => '/login', 'method' => 'POST', 'class' => 'login-form']) !!}
    <h3 class="form-title font-green">Sign In</h3>
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}</br>
        @endforeach
    </div>
    @endif
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label('email', 'Email', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::email('email', '', ['class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Email', 'autocomplete' => 'off']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Password', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::password('password', ['class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Password', 'autocomplete' => 'off']) !!}
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green uppercase">Login</button>
        <label class="rememberme check mt-checkbox mt-checkbox-outline">
            {!! Form::checkbox('remember', '1') !!} Remember
            <span></span>
        </label>
        <a href="/password/reset" id="forget-password" class="forget-password">Forgot Password?</a>
    </div>
    <div class="create-account">
        <p>
            <a href="/register" id="register-btn" class="uppercase">Create an account</a>
        </p>
    </div>
{!! Form::close() !!}
<!-- END LOGIN FORM -->
@endsection
