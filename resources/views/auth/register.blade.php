@extends('layouts.auth')

@section('form')
<!-- BEGIN REGISTRATION FORM -->
{!! Form::open(['url' => '/register', 'method' => 'POST', 'class' => 'register-form']) !!}
    <h3 class="font-green">Sign Up</h3>
    <p class="hint"> Enter your personal details below: </p>
    <div class="form-group">
        {!! Form::label('firstname', 'First Name', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::text('firstname', '', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'First Name']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('lastname', 'Last Name', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::text('lastname', '', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Last Name']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('company', 'Company', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::text('company', '', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Company']) !!}
    </div>
    <?php /*<div class="form-group">
        {!! Form::label('address', 'Address', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::text('address', '', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Address']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('city', 'City/Town', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::text('city', '', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'City/Town']) !!}
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Country</label>
        <select name="country" class="form-control">
            <option value="">Country</option>
            @foreach(Country::orderBy('name')->get() as $country)
                <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        {!! Form::label('postcode', 'Postcode', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::text('postcode', '', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Postcode']) !!}
    </div> */ ?>
    <p class="hint"> Enter your account details below: </p>
    <div class="form-group">
        {!! Form::label('email', 'Email', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::email('email', '', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Email']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Password', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::password('password', ['id' => 'register_password', 'class' => 'form-control placeholder-no-fix', 'placeholder' => 'Password']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password_confirmation', 'Re-type Your Password', ['class' => 'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Re-type Your Password']) !!}
    </div>
    <div class="form-actions">
        <button type="button" id="register-back-btn" class="btn green btn-outline">Back</button>
        <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
    </div>
{!! Form::close() !!}
<!-- END REGISTRATION FORM -->
@endsection