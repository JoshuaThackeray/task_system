<?php

namespace Alsek\Project\Repositories;

use Alsek\Project\Repositories\Task;
use App\User;
use Auth;

class TaskRepository extends Repository
{
    public function __construct()
    {
        
    }
    
    /**
     * Finds a project associated with the user by the project id.
     *
     * @var Integer
     * @return Alsek\Project\Models\ProjectUser Object
     */
    public function findById( Int $id )
    {
        return Task::find($id);
    }
    
    /**
     * Finds all tasks associated with the project id.
     *
     * @var Integer
     * @var Array
     * 
     * @return Array
     */
    public function getAllTasksForProject( Int $projectId, Array $fields = null )
    {
        $project = Auth::user()->projects()->find($projectId);
        
        return $project->model->tasks()->select(( is_null($fields) ? '*' : $fields))->with('creator')->get();
    }
}