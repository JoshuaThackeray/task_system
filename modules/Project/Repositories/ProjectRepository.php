<?php

namespace Alsek\Project\Repositories;

use Alsek\Project\Models\Project;
use Alsek\Project\Models\ProjectUser;
use App\User;

class ProjectRepository extends Repository
{
    public function __construct()
    {
        
    }
    
    /**
     * Finds a project associated with the user by the project id.
     *
     * @var Integer
     * @return Alsek\Project\Models\ProjectUser Object
     */
    public function findById( Int $id )
    {
        return Auth::user()->projects()->where('project_id', $id)->first();
    }
    
    /**
     * Finds a project associated with the user by the project id.
     *
     * @var Integer
     * @return Array
     */
    public function getAllProjectsByUserId( Int $id )
    {
        $user = User::find($id);
        
        return $user->projects;
    }
}