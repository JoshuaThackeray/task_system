<?php

namespace Alsek\Project\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Project extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'status', 'created_by'];
    
    const ROLE_STANDARD = 0;
    const ROLE_ADMIN = 1;
    
    /**
     * Relationships
     * 
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function users()
    {
        return $this->belongsToMany(User::class, 'project_user', 'project_id', 'user_id')->withPivot('role', 'status');
    }
    
    public function tasks()
    {
        return $this->hasMany(Task::class, 'project_id');
    }

}
