<?php

namespace Alsek\Project\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Task extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'task';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['priority', 'estimate', 'summary', 'description', 'status'];
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'created_at', 'summary', 'creator', 'status'];
    
    
    
    /**
     * Relationships
     * 
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function assignee()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }
    
    
}
