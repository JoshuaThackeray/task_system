<?php

namespace Alsek\Project\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ProjectUser extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_user';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['created_by', 'project_id', 'user_id', 'role', 'status'];
    
    const ROLE_STANDARD = 0;
    const ROLE_ADMIN = 1;
    
    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }
    
    /**
     * Relationships
     * 
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function model()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

}
