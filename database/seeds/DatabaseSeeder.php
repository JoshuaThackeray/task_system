<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesSeeder::class);
        
        $users = factory(App\User::class, 100)->create();
        $projects = factory(Alsek\Project\Models\Project::class, 10)->create()
            ->each(function($p){ 
                $userId = 81;//rand(1, 100);
                $p->users()->attach( User::find($userId), ['created_by' => 1, 'role' => rand(0,1)] );
                
                $user = User::find($userId);
                $user->update([
                    'current_project' => $p->id
                ]);
            });
        
        factory(Alsek\Project\Models\Task::class, 1200)->create();
    }
}
