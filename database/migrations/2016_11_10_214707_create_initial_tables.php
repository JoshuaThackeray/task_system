<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('created_by');
            $table->integer('assigned_to')->nullable();
            $table->integer('priority')->nullable();
            $table->string('estimate')->nullable();
            $table->string('summary');
            $table->string('description')->nullable();
            $table->boolean('status')->default(0);
            $table->integer('project_id')->nullable();
        });
        
        Schema::create('task_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('created_by');
            $table->string('comment');
            $table->boolean('edited')->default(0);
            $table->boolean('status')->default(1);
            $table->integer('task_id')->nullable();
        });
        
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('created_by');
            $table->integer('category_id')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->boolean('status')->default(1);
        });
        
        Schema::create('project_user', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('created_by');
            $table->integer('project_id');
            $table->integer('user_id');
            $table->integer('role');
            $table->boolean('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task');
        Schema::dropIfExists('task_comment');
        Schema::dropIfExists('project');
        Schema::dropIfExists('project_user');
    }
}
