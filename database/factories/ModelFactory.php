<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'firstname' => $faker->firstname,
        'lastname' => $faker->lastname,
        'company' => $faker->company,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'country' => $faker->firstname,
        'postcode' => $faker->postcode,
        'password' => $password ?: $password = bcrypt('Test2341'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Alsek\Project\Models\Project::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->text,
        'created_by' => rand(1,100)
    ];
});


$factory->define(Alsek\Project\Models\Task::class, function (Faker\Generator $faker) {
    return [
        'created_at' => $faker->dateTimeBetween('-2 years'),
        'created_by' => rand(1,100),
        'assigned_to' => rand(1,100),
        'priority' => rand(1,100),
        'estimate' => '1h 10m',
        'summary' => implode(" ", $faker->words(rand(2,4))),
        'description' => $faker->sentence,
        'status' => rand(0,2),
        'project_id' => rand(1,10)
    ];
});
